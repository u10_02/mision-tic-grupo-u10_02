const express = require('express');
const conectarDB = require('../config/db');
const cors = require('cors');

const app = express();
const port = 7000;


//conectar DB
conectarDB();
app.use(cors());
app.use(express.json());
app.use('/api/productos', require('../routes/producto'));
app.use('/api/usuario', require('../routes/usuario'));



//muestra mensaje en el navegador
app.get("/", (req, res) => {
    res.send("Bienvenidos, esta configurado su servidor");
});

app.listen(port, () => console.log('El servidor esta conectado', port));