const mongoose = require('mongoose');

const usuarioShema = mongoose.Schema({
    nombre: {
        type: String,
        require: true
    },

    email: {
        type: String,
        require: true
    },

    password: {
        type: String,
        require: true
    }

});

module.exports = mongoose.model('usuario', usuarioShema);