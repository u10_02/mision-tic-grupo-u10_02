const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    nombre: {
        type: String,
        require: true
    },
    marca: {
        type: String,
        require: true
    },
    tipo: {
        type: String,
        require: true
    },
    precio: {
        type: Number,
        require: true
    }
    
});

module.exports = mongoose.model('Producto', productoSchema);