const mongoose = require('mongoose');

require('dotenv').config();

const conectarDB = async () => {
    try {
        await mongoose.connect(process.env.MONGO_URL, {

        });
        console.log("La base de datos esta conectada ");
    }
    catch (error) {
        console.log(error);
        process.exit(1);//detenemoss la app
    }
}

module.exports = conectarDB;